# md3bib

`md3bib` is a small Python script to help compile bibliography files for `pandoc`/markdown writing endeavours.

It is called `md3bib` because it makes `.md` into `.bib` and [there already is an `md2bib.py`](https://github.com/reagle/pandoc-wrappers).

## Installation & Requirements

`md3bib` runs on all standard library Python (3.5).

To use the script, clone this repository to an appropriate folder.

For ease of use, symlinking the script to `~/bin` or equivalent is recommended.

## Usage

`md3bib` is fairly simple to use: 

* You give it markdown to make bibliographies from. `.md`files (as positional arguments) or `stdin` both work.
* You tell it where to put the result with the `-o`/`--output` flags.
	* If you specify a file that already exists, it will expand upon it.
	* If you omit the flag, it will write the bibliography to `stdout`.
* You tell it what Bibtex file(s) (or folders) to use as library with the `-l`/`--library` flag.
	* All the shell wildcards (like `*.bib`) get resolved by the shell, so they can be used to submit multiple library files.
	* Every folder gets trawled recursively for `*.bib` files.
	* If you omit the flag, a default folder is used. (For now, change the variable `DEFAULT_LIBPATH` in the script to change the folder.)

### Example

Make a bibliography file for the file `paper.md`:

	$ md3bib -o paper_biblio.bib paper.md

